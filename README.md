firstRobot 0.0.2

### What's Next? ###

I will be working on the body of the robot with URDF files.
So far it is just a robot brain that can add two numbers per the
ROS tutorials.

### What is this repository for? ###

This is the files comprising my first robot in ROS with the goal of getting it to work in gazebo.

### Who do I talk to? ###

Dillon Flohr <dillonflohr@yahoo.com>